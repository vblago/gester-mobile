import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:gester_mobile/presentation/res/assets.dart';

class LogoWidget extends StatelessWidget {
  final Color color;
  final MainAxisAlignment alignment;

  const LogoWidget({
    @required this.color,
    this.alignment = MainAxisAlignment.center,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: alignment,
      children: <Widget>[
        Image.asset(
          GesterAssets.logo,
          width: 50,
          height: 50,
        ),
        Text(
          "Gester",
          style: GoogleFonts.raleway(
            textStyle: Theme.of(context).textTheme.headline4,
            fontSize: 32,
            fontWeight: FontWeight.w700,
            color: color,
          ),
        ),
      ],
    );
  }
}
