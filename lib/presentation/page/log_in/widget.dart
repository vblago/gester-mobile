import 'package:gester_mobile/presentation/model/user.dart';
import 'package:gester_mobile/presentation/page/home/widget.dart';
import 'package:gester_mobile/presentation/page/sign_up/widget.dart';
import 'package:gester_mobile/presentation/res/colors.dart';
import 'package:gester_mobile/presentation/widget/logo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _emailTC =
  TextEditingController(text: "peter.petrenko@gmail.com");
  final TextEditingController _passwordTC =
  TextEditingController(text: "12345678");

  final FirebaseAuth auth = FirebaseAuth.instance;
  final Firestore firestore = Firestore.instance;

  Widget _entryField(
      String title,
      TextEditingController controller, {
        bool isPassword = false,
      }) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
              controller: controller,
              obscureText: isPassword,
              cursorColor: GesterColorPalette.greenDark,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  fillColor: Color(0xfff3f3f4),
                  filled: true))
        ],
      ),
    );
  }

  Widget _submitButton() {
    return CupertinoButton(
      padding: EdgeInsets.zero,
      minSize: 0,
      onPressed: _logIn,
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(15)),
          gradient: LinearGradient(
            begin: Alignment.centerLeft,
            end: Alignment.centerRight,
            colors: <Color>[
              GesterColorPalette.greenDark,
              GesterColorPalette.green
            ],
          ),
        ),
        child: Text(
          'Log In'.toUpperCase(),
          style: TextStyle(
            fontSize: 18,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Widget _createAccountLabel() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Don\'t have an account?',
            style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
          ),
          SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: () {
              Navigator.of(context).pop();
              Navigator.push<void>(
                  context,
                  MaterialPageRoute<void>(
                      builder: (BuildContext context) => SignUpPage()));
            },
            child: Text(
              'Sign Up',
              style: TextStyle(
                  color: GesterColorPalette.greenDark,
                  fontSize: 13,
                  fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
    );
  }

  Widget _emailPasswordWidget() {
    return Column(
      children: <Widget>[
        _entryField("Email", _emailTC),
        _entryField("Password", _passwordTC, isPassword: true),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          width: double.maxFinite,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: <Color>[
                    GesterColorPalette.lime,
                    GesterColorPalette.greenDark,
                  ])),
          height: MediaQuery.of(context).size.height,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: double.maxFinite,
                  constraints: BoxConstraints(maxWidth: 300),
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
//                _backButton(),
                      Expanded(
                        flex: 3,
                        child: SizedBox(),
                      ),
                      LogoWidget(
                        color: Colors.white,
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      _emailPasswordWidget(),
                      SizedBox(
                        height: 20,
                      ),
                      _submitButton(),
                      SizedBox(
                        height: 20,
                      ),
                      _createAccountLabel(),
                      Expanded(
                        flex: 2,
                        child: SizedBox(),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  Future<void> _logIn() async {
    AuthResult result = await auth.signInWithEmailAndPassword(
        email: _emailTC.text, password: _passwordTC.text);
    final FirebaseUser user = result.user;

    assert(user != null);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await auth.currentUser();
    assert(user.uid == currentUser.uid);

    final DocumentSnapshot snapshot =
    await firestore.collection("users").document(user.uid).get();
    List<String> availableRooms = List<String>();
    if (snapshot.data["availableRooms"] != null) {
      (snapshot.data["availableRooms"] as List<dynamic>)
          .forEach((dynamic room) => availableRooms.add(room as String));
    }
    if (availableRooms.isEmpty) {
      availableRooms = null;
    }
    Navigator.of(context)..pop()..pop();
    Navigator.push<void>(
        context,
        MaterialPageRoute<void>(
            builder: (BuildContext context) => HomeWidget(
              user: User(snapshot.data["name"] as String,
                  snapshot.data["email"] as String, availableRooms),
            )));
  }
}
