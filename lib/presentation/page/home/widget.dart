import 'package:gester_mobile/presentation/model/user.dart';
import 'package:gester_mobile/presentation/page/welcome/widget.dart';
import 'package:gester_mobile/presentation/res/assets.dart';
import 'package:gester_mobile/presentation/res/colors.dart';
import 'package:gester_mobile/presentation/widget/dialog/asset.dart';
import 'package:gester_mobile/presentation/widget/dialog/base.dart';
import 'package:gester_mobile/presentation/widget/logo.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeWidget extends StatefulWidget {
  final User user;

  const HomeWidget({
    @required this.user,
    Key key,
  }) : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  final TextEditingController _yawTC = TextEditingController();
  final TextEditingController _pitchTC = TextEditingController();
  final TextEditingController _rollTC = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: GesterColorPalette.greenDark,
        title: _buildAppBar(context),
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: 20),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            width: double.maxFinite,
            child: Text(
              "Available Equipment:",
              style: TextStyle(
                fontSize: 18,
                color: GesterColorPalette.green,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            child: _buildAvailableEquipment(),
          ),
        ],
      ),
    );
  }

  Widget _buildAppBar(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: LogoWidget(
            color: Colors.white,
            alignment: MainAxisAlignment.start,
          ),
        ),
        Text(
          widget.user.name,
          style: TextStyle(
            fontSize: 12,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            "•",
            style: TextStyle(
              fontSize: 12,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        CupertinoButton(
          padding: EdgeInsets.zero,
          minSize: 0,
          onPressed: () async {
            await FirebaseAuth.instance.signOut();
            Navigator.of(context).pop();
            Navigator.push<void>(
                context,
                MaterialPageRoute<void>(
                    builder: (BuildContext context) => WelcomePage()));
          },
          child: Text(
            "Log Out",
            style: TextStyle(
              fontSize: 12,
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildAvailableEquipment() {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection("equipment").snapshots(),
      builder: (_, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasData) {
          final List<DocumentSnapshot> equipment = snapshot.data.documents
              .where((DocumentSnapshot element) =>
                  element.data["assignToEmail"] != null &&
                  (element.data["assignToEmail"] as String) ==
                      widget.user.email)
              .toList();
          return ListView.builder(
            padding: const EdgeInsets.symmetric(
              vertical: 20,
              horizontal: 15,
            ),
            itemBuilder: (BuildContext context, int index) {
              return CupertinoButton(
                onPressed: () => _configEquipment(context, equipment[index]),
                padding: EdgeInsets.zero,
                child: Container(
                  color: index % 2 == 0 ? GesterColorPalette.lime : null,
                  height: 70,
                  width: double.maxFinite,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        equipment[index]["brand"] as String,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        equipment[index]["model"] as String,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                        ),
                      ),
                      Text(
                        equipment[index]["goal"] as String,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
            itemCount: equipment.length,
          );
        } else {
          return Center(
            child: SizedBox(
              width: 30,
              height: 30,
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );
  }

  void _configEquipment(BuildContext context, DocumentSnapshot document) {
    _yawTC.text = (document["yawTrigger"] as num).toString();
    _pitchTC.text = (document["pitchTrigger"] as num).toString();
    _rollTC.text = (document["rollTrigger"] as num).toString();
    showDialog<void>(
      context: context,
      builder: (_) => AssetGiffyDialog(
        image: Image.asset(
          GesterAssets.equipmentGifAnimation,
          height: 200,
        ),
        title: Text(
          'Configure equipment',
          style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
        ),
        description: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _entryField("Yaw", _yawTC),
              _entryField("Pitch", _pitchTC),
              _entryField("Roll", _rollTC),
            ],
          ),
        ),
        buttonOkText: Text(
          "Save",
          style: TextStyle(
            color: Colors.white,
          ),
        ),
        onOkButtonPressed: () async {
          await Firestore.instance
              .collection("equipment")
              .document(document.documentID)
              .updateData(<String, double>{
            "yawTrigger":
                _yawTC.text.isNotEmpty ? double.parse(_yawTC.text) : null,
            "rollTrigger":
                _rollTC.text.isNotEmpty ? double.parse(_rollTC.text) : null,
            "pitchTrigger":
                _pitchTC.text.isNotEmpty ? double.parse(_pitchTC.text) : null,
          });
          Navigator.pop(context);
        },
        entryAnimation: EntryAnimation.TOP,
      ),
    );
  }

  Widget _entryField(String title, TextEditingController controller) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: controller,
            cursorColor: GesterColorPalette.greenDark,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
              border: InputBorder.none,
              fillColor: Color(0xfff3f3f4),
              filled: true,
            ),
          ),
        ],
      ),
    );
  }
}
